O PhotoShareServer deverá correr com o server.policy associado e só aceita como argumentos um número de porto.
Ao executar pela primeira vez, será criada a directoria PhotoShareServer, que irá conter a informação sobre os diversos utilizadores e respectivas passes 
(userPass.txt).
Para cada utilizador que for registado, irá ser criada uma pasta. Esta irá conter um ficheiro que descreve os seguidores deste (followers.txt), 
as fotos que forem adicionadas e informação sobre as mesmas (nome da foto.txt), nomeadamente, a data de publicação, o numero de dislikes e likes, 
que seguidores gostaram da foto ( seguidor=true significa que o seguidor gostou da foto, se for igual a false significa que não gostou)
e os comentários que os seguidores fizeram.
O PhotoShare deverá correr com o client.policy associado. Aceita os argumentos que estão descritos no enunciando do trabalho.
Não é possível encadear comandos numa só execução (por ex: -a foto.jpg -L user foto2.jpg).
Para os comandos -a, -f e -r, a falha na parte do servidor ao processar um dos argumentos dados significa que os argumentos subsequentes não
 irão ser processados. Ao executar o comando -a, serão somente enviadas as fotos que estejam na directoria corrente (ie: A pasta onde se encontra 
o projecto). Ao executar o comando -g, as fotos irão ser descarregadas para o directório corrente, e irão ser criados ficheiros (nome da foto 
comments.txt) que irão conter os respectivos comentários das fotos.

O manUsers pede uma palavra passe para iniciar o seu uso, que é 512901. Está passe foi usada para gerar a chave existente dentro da keystore KeyStoreName.
O segundo argumento do PhotoShareServer terá de ser essa mesma palavra-passe.

O MAC calculado pelo ManUsers e pelo PhotoShareServer será escrito no ficheiro mac.txt.

Os ficheiros que contém os seguidores de um user (followers.txt) e a informação sobre as fotas será cifrada e assinada digitalmente. 

O manUsers fornece uma opção para apagar um determinado utilizador. Nesta implementação, apagar significa apenas que o nome do utilizador 
irá desaparecer do ficheiro userPass.txt, impedindo esse utilizador de se autenticar. No entanto, as suas fotos irão permanecer no servidor.

Configuração da VM no Eclipse:
	PhotoShareServer: -Djava.security.manager -Djava.security.policy=server.policy
	PhotoShare: -Djava.security.manager -Djava.security.policy=client.policy
