# Projecto SC

Projecto realizado no âmbito da disciplina de Segurança e Confiablidade, regido pelo Sr .Prof. Dr. Nuno Neves ,do curso de Engenharia Informática, no ano lectivo 2017/2018.
A primeira fase consistiu na elaboração de um sistema de partilha de fotografias, baseado numa arquitectura cliente-servidor.
Na segunda fase foram construidos vários mecanismo de segurança no projecto para garantir a segurança do sistema.
Numa terceira fase, foi usado o utilitário IPTables e o Snort para configurar a firewall do próprio computador e montar sistemas de alerta para tentativas de violação do sistema.

Tecnologias e Conceitos usados: MAC, KeyStores, SSL, Assinatura Digitais,
Cifra Simétrica e Assimétricas, Hashing de Passwords.

Realizado por:
Francisco Loja
Paulo Pereiros