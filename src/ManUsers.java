
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.KeyStore.SecretKeyEntry;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.xml.bind.DatatypeConverter;

public class ManUsers {

	private static UserCatalog uc = new UserCatalog();
	private static SecretKey macKey;
	private static String home = System.getProperty("user.dir") + "/PhotoShareServer";;
	private static String authfile   = home + "/userPass.txt";;
	private static Random rand = new Random();
	public static void main(String args[]) {
		uc.loadUsers();
		System.out.println("Insira a palavra passe");
		Scanner reader = new Scanner(System.in);
		String password = reader.nextLine();
		macKey = getMacKey(password);
		if(macKey == null) {
			System.out.println("Não inseriu a palavra passe correta. O programa irá encerrar");
			reader.close();
			return;
		}
		File f = new File("mac.txt");
		if(!f.exists() || f.length() < 32) {
			System.out.println("Sem MAC definido. A gerar um ");
			createMacFile(macKey);
		}else {
			if(!confirmIntegrity(macKey)) {
				System.out.println("Possivel quebra de integridade. O programa irá encerrar");
				reader.close();
				return;
			}
		}
		boolean running = true;
		String userName = null;
		String userPassword = null;
		User u = null;
		while(running) {
		System.out.println("Prima a para adicionar um utilizador, r para remover, u para alterar a password de um utilizador ou q para sair do programa");
		String option = reader.nextLine();
		switch(option) {

			case("a"):
				System.out.println("Insira o nome do utilizador a adicionar");
				userName = reader.nextLine();
				System.out.println("Insira a password");
				userPassword = reader.nextLine();
				addUser(userName, userPassword);
				System.out.println("User " + userName+  " adicionado");
				break;
			case("r"):
				System.out.println("Insira o nome do utilizador a remover");
				userName = reader.nextLine();
				u = uc.getUser(userName);
				if(u != null)
					updateUser(u,"",true);
				else
					System.out.println("Utilizador " + userName + " não existe.");
				System.out.println("User " + userName+  " removido");
				break;
			case("u"):
				System.out.println("Insira o nome do utilizador a alterar");
				userName = reader.nextLine();
				System.out.println("Insira a password");
				userPassword = reader.nextLine();
				u = uc.getUser(userName);
				if(u != null)
					updateUser(u,userPassword, false);
				else
					System.out.println("Utilizador " + userName + " não existe.");
				System.out.println("Password de  utilizador " + userName + " alterada" );
				break;
			case("q"):
				System.out.println("O programa irá encerrar.");
			running = false;
			break;
			default:
				System.out.println("Não inseriu um comando válido. Por fazor, tente outra vez");
				break;
			}
		}
		reader.close();
	}

	private static byte[] readMacFile() {
		byte[] mac = new byte[32]; //256 a dividir por 8

		FileInputStream fis = null;
		try {
			fis = new FileInputStream("mac.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(fis);

			ois.readFully(mac,0,32);

			ois.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mac;
	}

	private static void createMacFile(SecretKey key) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream("mac.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(fos);

			byte mac[]= generateMac(key);

			oos.write(mac);
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void addUser(String userName, String password) {
    	uc.addUser(userName, password);
    	createMacFile(macKey);
    }
    
    public static void updateUser(User u, String password, boolean remove) {
    	uc.updateUser(u,password,remove);
    	createMacFile(macKey);
    }
    
    public static boolean authenticate(String user, String pass){
    	return uc.authenticate(user, pass);
    }
    public static byte[] generateMac(SecretKey k){
		Mac mac = null;
		try {
			mac = Mac.getInstance("HmacSHA256");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			mac.init(k);
		} catch (InvalidKeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Path path = Paths.get(authfile);
		byte[] data = null;
		try {
			data = Files.readAllBytes(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mac.update(data);

		return mac.doFinal();
	}
    
    
    public static SecretKey generatePBEKey(String password, byte[] salt) {
    	
    	PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 128,64*8); // pass, salt, iterations
		SecretKeyFactory kf = null;
		try {
			kf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SecretKey key = null;
		try {
			key = kf.generateSecret(keySpec);
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	return key;
    }
    
    public static byte[] generateSalt() {
    	byte[] salt = new byte[8];
    	rand.nextBytes(salt);
		return salt;
    }
    





	public static SecretKey getMacKey(String password) {

		KeyStore ks = null;
		try {
			ks = KeyStore.getInstance("JCEKS");
		} catch (KeyStoreException e1) {
			e1.printStackTrace();
		}

		// get user password and file input stream

	    FileInputStream fis = null;
	    try {
	        try {
				fis = new FileInputStream("keyStoreName");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			ks.load(fis, password.toCharArray());
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			return null;
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		KeyStore.ProtectionParameter protParam = new KeyStore.PasswordProtection(password.toCharArray());
		KeyStore.SecretKeyEntry pkEntry = null;
		try {
			pkEntry = (SecretKeyEntry)
					ks.getEntry("secretKeyAlias", protParam);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableEntryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SecretKey k = pkEntry.getSecretKey();
		return k;

	}
	public static boolean confirmIntegrity(SecretKey key) {
		byte lastMac[] = readMacFile();
		byte currentMac[] = generateMac(key);
		return Arrays.equals(lastMac, currentMac);
	}

}
