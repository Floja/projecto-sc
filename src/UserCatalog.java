import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class UserCatalog {
	private static UserCatalog instance = new UserCatalog();
	private ArrayList<User> users;
	private BufferedReader userPassReader;
	private BufferedWriter userPassWriter;
	private String home;
	private String authfile;
	
	/*
	 *  Cria lista com todos utilizadores se existir userPass.txt. 
	 *  Caso contrario, cria esse ficheiro
	 */
	public UserCatalog() {
		home = System.getProperty("user.dir") + "/PhotoShareServer";
		authfile = home + "/userPass.txt";
		users = new ArrayList<User>();
	}
	
	public static UserCatalog getInstance() {
		return instance;
	}
	
	public void loadUsers() {
		try {
			userPassWriter = new BufferedWriter (new FileWriter (authfile, true));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		File userPassInfo = new File(authfile);
		if(userPassInfo.exists() && !userPassInfo.isDirectory()) {
			
				String currLine = null;
				//Procura nome de utilizador no ficheiro
				try {
					userPassReader = new BufferedReader ( new FileReader (authfile));
					while( (currLine = userPassReader.readLine()) != null) {
						String userPass [] = currLine.split(":");
						User newUser = new User(userPass[0]);
						users.add(newUser);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				
		}else {
				try {
					userPassWriter.write("");
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		try {
			userPassWriter.close();
			userPassReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public User getUser(String userName) {
		for(User u: users) {
			if(u.getName().equals(userName))
				return u;
		}
		
		return null;
	}

	public synchronized boolean hasUser(User u) {
		return users.contains(u);
	}
	
	//Adiciona utilizador � lista e escreve nome de utilizador
	
	public boolean authenticate(String userName, String password) {
		try {
			userPassReader = new BufferedReader ( new FileReader (authfile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String currLine = null;
		try {
			while( (currLine = userPassReader.readLine()) != null) {
				String userInfo[] = currLine.split(":");
				if(userName.equals(userInfo[0])) {
					
					byte [] salt = DatatypeConverter.parseBase64Binary(userInfo[1]);
					
					byte [] encriptedPassword = DatatypeConverter.parseBase64Binary(userInfo[2]);
					
					SecretKey key = ManUsers.generatePBEKey(password,salt); 
					byte[] encodedKey = key.getEncoded();
					return Arrays.equals(encodedKey, encriptedPassword);
				}
					
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	
	public void addUser(String userName, String password) {
		
		
		try {
			
			userPassWriter = new BufferedWriter (new FileWriter (authfile, true));
			userPassWriter.write(userName+ ":" + hash(password) );
			userPassWriter.newLine();
			userPassWriter.close();
			
			Files.createDirectories(Paths.get(home + "/"+userName));
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
		try {
			users.add(new User(userName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String hash( String password) {
		byte[] salt = ManUsers.generateSalt();
		SecretKey key = ManUsers.generatePBEKey(password,salt);
		String saltString = DatatypeConverter.printBase64Binary(salt);
		String encodedKey = DatatypeConverter.printBase64Binary(key.getEncoded());
		return saltString + ":" + encodedKey;
	}
	

	public void updateUser(User user, String newPassword, boolean remove) {
		for(User u: users) {
			if(u.getName().equals(user.getName())) {
				int index = users.indexOf(u);
				users.set(index, user);
				break;
			}
		}
			try {
				File inputFile = new File(authfile);
				File tempFile = new File(authfile + "temp");
				userPassReader = new BufferedReader(new FileReader(inputFile));
				userPassWriter = new BufferedWriter(new FileWriter(tempFile));
				String currLine = null;
				while( (currLine = userPassReader.readLine()) != null) {
					String userInfo[] = currLine.split(":");
					if(userInfo[0].equals(user.getName())) {
						if(!remove) {
							userPassWriter.write(user.getName() +":" + hash(newPassword));
							userPassWriter.newLine();
						}
					}else {
						userPassWriter.write(currLine);
						userPassWriter.newLine();
					}
				}
				
				
				userPassReader.close();
				userPassWriter.close();
				Files.delete(inputFile.toPath());
				Files.move(tempFile.toPath(), inputFile.toPath(), StandardCopyOption.REPLACE_EXISTING );
				
			} catch (IOException e) {
			}
			
			
	}
	
	
	
	public boolean loadFollowers(User leader) throws IOException {
		return leader.loadFollowers(this);
	}
	
	public void addFollower(User leader, User follower) throws IOException {
		leader.addFollower(follower);
		
	}

	public void removeFollower(User leader, User follower) throws IOException {
		leader.removeFollower(follower);
	}
	
	public boolean loadPhotos(User leader) {
		
		return leader.loadPhotos();
		
	}
	
	public void addNewPhoto(User currentUser, String photoName, String extension, Date date) throws IOException {
		currentUser.addNewPhoto(photoName, extension, date);
	}

	public boolean addLike(User leader, String followerName, Photo p) {
		boolean notLiked = leader.addLike(p, followerName);

		return notLiked;
	}

	public boolean addDislike(User leader, String followerName, Photo p) {
		boolean notDisliked = leader.addDislike(p, followerName);
		return notDisliked;
	}

	public boolean addComment(User leader, String followerName, Photo p, String comment) {
		boolean commented = leader.addComment(p, followerName, comment);
		return commented;
	}
	
}
