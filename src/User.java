import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

public class User {
	Communication comm;
	private static String homePath;
	private String name;
	private ArrayList<User> followers;
	private ArrayList<Photo> photos;
	
	public User(String name) throws IOException {
		this.name = name;
		this.followers = new ArrayList<User>();
		this.photos = new ArrayList<Photo>();
		this.comm = Communication.getInstance();
	}
	
	public static void setHomePath(String home) {
		homePath = home;
	}
	
	
	public boolean loadPhotos() {
		photos.clear();
		File f = new File(homePath + "/" + name);
		
		FilenameFilter textFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				String lowercaseName = name.toLowerCase();
				if (lowercaseName.endsWith(".txt") || lowercaseName.endsWith(".key") ||  lowercaseName.endsWith(".sig")) {
					return false;
				} else {
					return true;
				}
			}
		};
		
		File[] files = f.listFiles(textFilter);
		if(files != null) {
			for(File temp : files) {
					String currLine = null;
					//Procura nome de utilizador no ficheiro
					Photo p;
					try {
						
						//Tira a extensao do nome do ficheiro
						int pointIndex = temp.getName().indexOf(".");
						String photo = temp.getName();
						String photoName = photo.substring(0, pointIndex);
						String extension = photo.substring(pointIndex);
						
						boolean sucess = comm.verifySign(homePath+ "/" + name + "/" + photoName + ".txt");
						if(!sucess)
							return false;
						
						CipherInputStream cis = comm.getCipherInputStream(homePath+ "/" + name + "/" + photoName + ".txt");
						BufferedReader photoReader = new BufferedReader ( new InputStreamReader(cis));
						String date = photoReader.readLine();
						
						currLine = photoReader.readLine();
						String likesDislikes [] = currLine.split(":");
						int likes = Integer.parseInt(likesDislikes[0]);
						int dislikes = Integer.parseInt(likesDislikes[1]);
						
						HashMap<String, Boolean> followerOpinions = new HashMap <String,Boolean>();
						
						currLine = photoReader.readLine(); //Says Opinions
						currLine = photoReader.readLine();
							if(!currLine.equals("")) {
								
								String opinions [] = currLine.split(",");
								
								for(String opinion: opinions) {
									String followerOpinion [] = opinion.split("=");
									followerOpinions.put(followerOpinion[0], Boolean.valueOf(followerOpinion[1]));
								}
							}
							
							currLine = photoReader.readLine(); //Says comments
							currLine = photoReader.readLine();
 							ArrayList<String> comments = new ArrayList<String>();
							while(currLine != null) {
								
								comments.add(currLine);
								currLine = photoReader.readLine();
							}
							p = new Photo(photoName, extension, this, date, likes, dislikes, comments, followerOpinions);
						
						
						
						photos.add(p);
						photoReader.close();
						}
					 catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}
		}
		return true;
	}
	public boolean loadFollowers(UserCatalog users) {
		
		followers.clear();
		File followerPath = new File(homePath + "/" + this.name + "/followers.txt");
		if(followerPath.exists()) {
			boolean sucess = comm.verifySign(homePath + "/" + this.name + "/followers.txt");
			if(!sucess)
				return false;
			
			CipherInputStream cis = comm.getCipherInputStream(homePath + "/" + this.name + "/followers.txt");
			BufferedReader followerReader = new BufferedReader ( new InputStreamReader(cis));
			String currLine = null;
			try {
				currLine = followerReader.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			while(currLine != null) {
				User follower = users.getUser(currLine);
				
				try {
					this.addFollower(follower);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					currLine = followerReader.readLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			try {
				followerReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
			CipherOutputStream cos = comm.getCipherOutputStream(homePath + "/" + this.name + "/followers.txt");
			BufferedWriter followerWriter = new BufferedWriter(new OutputStreamWriter (cos));
			try {
				followerWriter.write("");
				followerWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			comm.signFile(homePath + "/" + this.name + "/followers.txt");
			
		}
		
		return true;
	}

	public void addFollower(User follower) throws IOException {
		if(!hasFollower(follower))
			followers.add(follower);
		registerFollowers();
	}
	
	public void removeFollower(User follower) throws IOException {
		followers.remove(follower);
		registerFollowers();
	}
	
	public void registerFollowers() throws IOException {
		CipherOutputStream cos = comm.getCipherOutputStream(homePath + "/" + this.getName() + "/followers.txt");
		BufferedWriter followerWriter = new BufferedWriter (new OutputStreamWriter (cos));
		for(User follower: followers) {
			followerWriter.write(follower.getName());
			followerWriter.newLine();
		}
		followerWriter.close();
		
		comm.signFile(homePath + "/" + this.getName() + "/followers.txt");
	}
	
	
	public boolean hasFollower(User follower) {
		return followers.contains(follower);
	}
	
	public String getName() {
		return this.name;
	}
	
	
	public void addNewPhoto(String photoName, String extension, Date date) throws IOException {
		
		Photo p = new Photo(photoName, extension, this, date.toString());
		photos.add(p);
		CipherOutputStream cos = comm.getCipherOutputStream(homePath + "/" + this.getName() + "/" + photoName + ".txt");
		BufferedWriter photoWriter = new BufferedWriter (new OutputStreamWriter (cos));
		photoWriter.write(date.toString());
		photoWriter.newLine();
		photoWriter.write(0 + ":" + 0 );
		photoWriter.newLine();
		photoWriter.write("Opinions");
		photoWriter.newLine();
		photoWriter.write("");
		photoWriter.newLine();
		photoWriter.write("Comments");
		photoWriter.newLine();
		photoWriter.close();
		
		comm.signFile(homePath + "/" + this.getName() + "/" + photoName + ".txt");
	}
	
	public boolean hasPhoto(String photoName) {
		for(Photo p: photos) {
			if(p.getName().equals(photoName))
				return true;
		}
		return false;
	}
	public Photo getPhoto(String photoName) {
		for(Photo p: photos) {
			if(p.getName().equals(photoName))
				return p;
		}
		return null;
	}
	
	public int getNumberPhotos(){
		return photos.size();
	}
	
	public ArrayList<Photo> getListPhotos(){
		return photos;
	}
	
	public int getNumberComments(String photoName) {
		for(Photo p: photos) {
			if(p.getName().equals(photoName))
				return p.getComments().size();
		}
		return 0;
	}
	
	public boolean addLike(Photo p, String followerName) {
		
		int photoIndex = photos.indexOf(p);
		boolean notLiked = p.addLike(followerName);
		photos.set(photoIndex, p);
		try {
			registerPhoto(p);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return notLiked;
	}
	
	public boolean addComment(Photo p, String followerName, String comment) {
		
		int photoIndex = photos.indexOf(p);
		p.addComment(followerName, comment);
		photos.set(photoIndex, p);
		try {
			registerPhoto(p);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean addDislike(Photo p, String followerName) {
		
		int photoIndex = photos.indexOf(p);
		boolean notDisliked = p.addDislike(followerName);
		photos.set(photoIndex, p);
		try {
			registerPhoto(p);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return notDisliked;
	}
	
	public void registerPhoto(Photo p) throws IOException {
		
			String photoName = p.getName();
			CipherOutputStream cos = comm.getCipherOutputStream(homePath + "/" + this.getName() + "/" + photoName + ".txt");
			BufferedWriter photoWriter = new BufferedWriter (new OutputStreamWriter (cos));
			photoWriter.write(p.getDate());
			photoWriter.newLine();
			photoWriter.write(p.getLikes() + ":" + p.getDislikes());
			photoWriter.newLine();
			photoWriter.write("Opinions");
			photoWriter.newLine();
			HashMap<String,Boolean> opinions = p.getOpinions();
			Set<Entry<String, Boolean>> s = opinions.entrySet();
			if(s.isEmpty()) {
				photoWriter.write("");
			}
			for(Entry<String, Boolean> e: s) {
				photoWriter.write(e.getKey() + "=" + e.getValue() + ",");
				
			}
			photoWriter.newLine();
			photoWriter.write("Comments");
			photoWriter.newLine();
			for(String comment: p.getComments()) {
				photoWriter.write(comment);
				photoWriter.newLine();
			}
				
			
			//TODO Registar comentarios
			photoWriter.close();
		
			
			comm.signFile(homePath + "/" + this.getName() + "/" + photoName + ".txt");
			
			
	}

	
}
