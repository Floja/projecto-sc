import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

public class PhotoShareServer {
	private static Communication comm;
	private static UserCatalog users;
	private static String home;


	public static void main(String[] args) {
		System.setProperty("javax.net.ssl.keyStore", "myServer.keyStore");
		System.setProperty("javax.net.ssl.keyStorePassword", "123456");
		comm = Communication.getInstance();
		if(args.length == 0) {
			System.out.println("Please insert a port number");
			return;
		}
		// Caminho para ficheiro onde estao guardadadas as passes dos utilizadores
		home = System.getProperty("user.dir") + "/PhotoShareServer";
		User.setHomePath(home);
		try {
			Files.createDirectories(Paths.get(home));
		} catch (IOException e) {
			e.printStackTrace();
		}
		users = UserCatalog.getInstance();
		System.out.println("servidor: main");
		PhotoShareServer server = new PhotoShareServer();
		int port = 0;
		try {
			port = Integer.parseInt(args[0]);
		}catch(NumberFormatException e){
			System.out.println("Insert a valid port");
			return;
		}
		
		String password = args[1];
		SecretKey macKey = ManUsers.getMacKey(password);
		boolean integrity  = ManUsers.confirmIntegrity(macKey);
		if(!integrity) {
			System.out.println("Quebra de integridade. A terminar sessão");
			return;
		}
		
		server.startServer(port);
	}
	class ServerThread extends Thread {

		private Socket socket = null;

		ServerThread(Socket inSoc) {
			socket = inSoc;
			System.out.println("thread do server para cada cliente");
		}

		public void run(){
			try {

				comm.setStream(socket);

				String user = null;
				String passwd = null;

				try {
					user = comm.readString();
					passwd = comm.readString();
					System.out.println("thread: depois de receber a password e o user");
				}catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
				users.loadUsers();
				User currentUser = users.getUser(user);
				
				boolean authenticated = ManUsers.authenticate(user, passwd);

				System.out.println("Authenticated: " + authenticated);

				if (authenticated){
					comm.writeBoolean(true);
				}
				else {
					comm.writeBoolean(false);
				}
				String command = null;

				ReentrantLock userLock = new ReentrantLock();	

				ReentrantLock followerLock = new ReentrantLock();	
				followerLock.lock();
				boolean followerIntegrity = users.loadFollowers(currentUser);
				followerLock.unlock();
				
				System.out.println("Carregados seguidores com sucesso: " + followerIntegrity  );
				
				
				
				
				
				
				ReentrantLock photosLock = new ReentrantLock();	
				photosLock.lock();
				boolean commentsIntegrity =users.loadPhotos(currentUser);
				photosLock.unlock();
				
				System.out.println("Carregados comentários com sucesso: " + commentsIntegrity  );
				
				comm.writeBoolean(followerIntegrity && commentsIntegrity);
				String followerName;
				User follower;
				String leaderName;
				User leader;
				String fileName;
				String photoName;
				String comment;
				boolean clientConnected = true;
				boolean isFollower = false;
				while(clientConnected && followerIntegrity && commentsIntegrity) {
					userLock.lock();
					users.loadUsers();
					userLock.unlock();
					try {
						command = comm.readString();
						System.out.println(command);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					switch(command) {
					case("-a"):
						userLock.lock();
					fileName = comm.readString();
					int firstPoint = fileName.indexOf(".");
					//photoName � o nome da foto sem as extens�es(jpg, png, etc)
					photoName = fileName.substring(0, firstPoint);
					String extension = fileName.substring(firstPoint);
					photosLock.lock();
					Boolean photoNotExists =!currentUser.hasPhoto(photoName);
					comm.writeBoolean(photoNotExists);
					if(photoNotExists){
						users.addNewPhoto(currentUser, photoName, extension, new Date());
						comm.receiveFile(home + "/" + currentUser.getName() + "/" + fileName, true);
					}
					photosLock.unlock();
					userLock.unlock();
					break;

					case("-c"):
						userLock.lock();
					leaderName = comm.readString();
					followerLock.lock();
					photosLock.lock();
					leader = users.getUser(leaderName);
					if(!users.loadFollowers(leader)) {
						System.out.println("Quebra de Integridade. A desconectar");
					}
					users.loadPhotos(leader);
					if(leader != null) {
						if(leaderName.equals(currentUser.getName())){
							isFollower = true;
						}
						else{
							isFollower = leader.hasFollower(currentUser);
						}
						comm.writeBoolean(isFollower);
						if(isFollower) {
							fileName = comm.readString();
							int firstPoint2 = fileName.indexOf(".");
							//photoName � o nome da foto sem as extens�es(jpg, png, etc)
							String photoName2 = fileName.substring(0, firstPoint2);
							Boolean hasPhoto = leader.hasPhoto(photoName2);
							comm.writeBoolean(hasPhoto);
							if(leader.hasPhoto(photoName2)) {
								Photo p = leader.getPhoto(photoName2);
								comment = comm.readString();
								users.addComment(leader,currentUser.getName(), p, comment);

							}
						}
					}else {
						//Follower nao existe
						comm.writeBoolean(false);
					}
					userLock.unlock();	
					followerLock.unlock();
					photosLock.unlock();
					break;

					case("-f"):
						userLock.lock();
					followerName = comm.readString();
					followerLock.lock();
					follower = users.getUser(followerName);
					if(follower != null) {

						Boolean isNotFollower = !currentUser.hasFollower(follower);
						comm.writeBoolean(isNotFollower);
						if(isNotFollower) {
							users.addFollower(currentUser, follower);
						}
					}
					else {
						comm.writeBoolean(false);
					}
					followerLock.unlock();
					userLock.unlock();
					break;

					case("-r"):
						userLock.lock();
					followerName = comm.readString();
					followerLock.lock();
					follower = users.getUser(followerName);
					if(follower != null) {
						isFollower = currentUser.hasFollower(follower);
						comm.writeBoolean(isFollower);
						if(isFollower) {
							users.removeFollower(currentUser, follower);
						}
					}
					followerLock.unlock();
					userLock.unlock();
					break;

					case("-L"):
						userLock.lock();
					leaderName = comm.readString();
					followerLock.lock();
					photosLock.lock();
					leader = users.getUser(leaderName);
					users.loadFollowers(leader);
					users.loadPhotos(leader);
					if(leader != null) {
						isFollower = leader.hasFollower(currentUser);
						comm.writeBoolean(isFollower);
						if(isFollower) {
							fileName = comm.readString();
							int firstPoint2 = fileName.indexOf(".");
							//photoName � o nome da foto sem as extens�es(jpg, png, etc)
							String photoName2 = fileName.substring(0, firstPoint2);
							Boolean hasPhoto = leader.hasPhoto(photoName2);
							comm.writeBoolean(hasPhoto);
							if(hasPhoto) {
								Photo p = leader.getPhoto(photoName2);
								boolean notLiked = users.addLike(leader,currentUser.getName(), p);
								comm.writeBoolean(notLiked);
							}
						}
					}else {
						//Leader nao existe
						comm.writeBoolean(false);
					}
					userLock.unlock();	
					followerLock.unlock();
					photosLock.unlock();
					break;

					case("-l"):
						userLock.lock();
					leaderName = comm.readString();
					followerLock.lock();
					photosLock.lock();
					leader = users.getUser(leaderName);
					users.loadFollowers(leader);
					users.loadPhotos(leader);
					if(leader != null) {
						if(leaderName.equals(currentUser.getName())){
							isFollower = true;
						}
						else{
							isFollower = leader.hasFollower(currentUser);
						}
						comm.writeBoolean(isFollower);
						if(isFollower) {
							int numberOfPhotos = leader.getNumberPhotos();
							comm.writeInt(numberOfPhotos);
							for(Photo p: leader.getListPhotos()){
								comm.writeString(p.getName());
								comm.writeString(p.getDate());
							}

						}
					}else {
						comm.writeBoolean(false);
					}
					userLock.unlock();	
					followerLock.unlock();
					photosLock.unlock();
					break;

					case("-i"):
						userLock.lock();
					leaderName = comm.readString();
					followerLock.lock();
					photosLock.lock();
					leader = users.getUser(leaderName);
					users.loadFollowers(leader);
					users.loadPhotos(leader);
					if(leader != null) {
						isFollower = false;
						if(leaderName.equals(currentUser.getName())){
							isFollower = true;
						}
						else{
							isFollower = leader.hasFollower(currentUser);
						}
						
						comm.writeBoolean(isFollower);
						if(isFollower) {
							fileName = comm.readString();
							int firstPoint2 = fileName.indexOf(".");
							//photoName � o nome da foto sem as extens�es(jpg, png, etc)
							String photoName2 = fileName.substring(0, firstPoint2);
							Boolean hasPhoto = leader.hasPhoto(photoName2);
							comm.writeBoolean(hasPhoto);
							if(hasPhoto){
								Photo p = leader.getPhoto(photoName2);
								int numberOfComments = leader.getNumberComments(photoName2);
								comm.writeInt(numberOfComments);
								for(String s:p.getComments()){
									comm.writeString(s);
								}
								comm.writeInt(p.getLikes());
								comm.writeInt(p.getDislikes());
							}
						}
						else{
							System.out.println("You need to follow to get "
									+ "the status of the photo");
							break;
						}

					}
					userLock.unlock();	
					followerLock.unlock();
					photosLock.unlock();
					break;

					case("-g"):
						userLock.lock();
					leaderName = comm.readString();
					followerLock.lock();
					photosLock.lock();
					leader = users.getUser(leaderName);
					users.loadFollowers(leader);
					users.loadPhotos(leader);
					if(leader != null) {
						if(leaderName.equals(currentUser.getName())){
							isFollower = true;
						}
						else{
							isFollower = leader.hasFollower(currentUser);
						}
						comm.writeBoolean(isFollower);
						if(isFollower) {
							comm.writeInt(leader.getNumberPhotos());
							for(Photo p:leader.getListPhotos()){

								fileName = p.getName() + p.getExtension();
								comm.writeString(fileName);
								comm.sendFile(home + "/" + leader.getName() + "/" + fileName, true);
								comm.writeInt(leader.getNumberComments(p.getName()));
								for(String s:p.getComments()){
									comm.writeString(s);
								}
							}
						}
						else{
							System.out.println("You need to follow to get "
									+ "all the photos and comments");
							break;
						}

					}
					userLock.unlock();	
					followerLock.unlock();
					photosLock.unlock();
					break;

					case("-D"):
						userLock.lock();
					leaderName = comm.readString();
					followerLock.lock();
					photosLock.lock();
					leader = users.getUser(leaderName);
					users.loadFollowers(leader);
					users.loadPhotos(leader);
					if(leader != null) {
						isFollower = leader.hasFollower(currentUser);
						comm.writeBoolean(isFollower);
						if(isFollower) {
							fileName = comm.readString();
							int firstPoint2 = fileName.indexOf(".");
							//photoName eh o nome da foto sem as extensoes(jpg, png, etc)
							String photoName2 = fileName.substring(0, firstPoint2);
							Boolean hasPhoto = leader.hasPhoto(photoName2);
							comm.writeBoolean(hasPhoto);
							if(hasPhoto) {
								Photo p = leader.getPhoto(photoName2);
								boolean notDisliked = users.addDislike(leader, currentUser.getName(), p);
								comm.writeBoolean(notDisliked);
							}
						}
					}else {
						//Follower nao existe
						comm.writeBoolean(false);
					}
					userLock.unlock();

					case("quit"):
						clientConnected = false;
					break;


					default:
						System.out.println("Please insert a valid command");
						break;

					}
				}
				comm.closeStream();
				socket.close();				
			}catch(IOException | ClassNotFoundException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {

			}
		}


	}
	public void startServer (int port){
		ServerSocketFactory ssf = SSLServerSocketFactory.getDefault();

		try (SSLServerSocket ss = (SSLServerSocket) ssf.createServerSocket(port)){
			while(true) {

				Socket inSoc = ss.accept();
				ServerThread newServerThread = new ServerThread(inSoc);
				newServerThread.start();
				

			}

		}catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}


}
