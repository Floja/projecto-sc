import java.io.IOException;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import javax.crypto.NoSuchPaddingException;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class PhotoShare{

	private static SSLSocket echoSocket;
	private static Communication comm;
	
	public static void main(String[] args) throws IOException, ClassNotFoundException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		System.setProperty("javax.net.ssl.trustStore","myClient.keyStore");
		final String userID;
		String[] adress;
		String pass;
		String command = "quit";
		int index;
		
		if(args.length == 0){
			System.out.println("Please insert a command");
			return;
		}
		if(args.length == 1) {
			System.out.println("Please insert the address");
			return;
		}
		userID = args[0];
		if(args[1].contains(":")) {
			Scanner reader = new Scanner(System.in);
			adress = args[1].split(":");
			System.out.println("Please insert your password");
			pass = reader.nextLine();
			index = 3;
			if(args.length >= 3)
				command = args[2];
			reader.close();
		}else if(args.length >= 3){
			pass = args[1];
			if(!args[2].contains(":")) {
				System.out.println("You did not insert a valid server adress");
				return;
			}
			adress = args[2].split(":");
			index = 4;
			if(args.length >= 4)
				command = args[3];
		}else {
			System.out.println("Please insert the address");
			return;
		}

		SocketFactory sf = SSLSocketFactory.getDefault( );
		echoSocket = null;
		final int serverPort = Integer.parseInt(adress[1]);
		try {
			echoSocket = (SSLSocket) sf.createSocket(adress[0], serverPort);
		} catch (UnknownHostException e1) {
			System.out.println(e1.getMessage());
			return;
		} catch (IOException e1) {
			System.out.println(e1.getMessage());
			return;
		}
		comm = Communication.getInstance();
		try {
			
			comm.setStream(echoSocket);
		}catch(IOException e) {
			System.out.println("Lost connection with server");
			e.printStackTrace();
			closeConnections();
			return;
		}
		try {
			comm.writeString(userID);
			comm.writeString(pass);
		} catch (IOException e) {
			closeConnections();
			return;
		}
		boolean fromServer = false;
		try {
			fromServer = comm.readBoolean();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(!fromServer) {
			System.out.println("Server could not authenticate or register you");
			closeConnections();
			return;
		}
		
		
		boolean integrity = false;
		try {
			integrity = comm.readBoolean();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(!integrity) {
			System.out.println("Detectada falha de integridade. A fechar a conexão");
			closeConnections();
			return;
		}

		Boolean notExists = true;
		Boolean notFollower = true;
		boolean isFollower;
		Boolean hasPhoto = true;
		String leaderName;
		String photoName;
		int numberOfPhotos;
		String comments;
		int numberOfComments;
		switch(command){
			case("-a"):
				if(index == args.length) {
					System.out.println("Please insert photos names");
					break;
				}
				while(index < args.length){
						
					String fileName = args[index];
					if(!comm.fileExists(fileName)) {
						System.out.println("Please insert valid photos");
						break;
					}
					comm.writeString("-a");
					comm.writeString(args[index]);
					notExists = comm.readBoolean();
					if(notExists){
						comm.sendFile(fileName, false);
						index++;
					}
					else{
						System.out.println("Photo " + args[index] + " already exists");
						break;
					}
					
				}
				break;
			case "-l":
				if(args.length == index) {
					System.out.println("You must insert the user you want the photos info from");
					break;
				}
				comm.writeString("-l");
				leaderName = args[index];
				comm.writeString(leaderName);
				boolean isFollower2 = comm.readBoolean();
				if(!isFollower2) {
					System.out.println("You do not follow " + leaderName);
					break;
				}

				numberOfPhotos = comm.readInt();
				if(numberOfPhotos == 0)
					System.out.println("User does not have photos");
				for(int i = 0; i < numberOfPhotos; i++) {
					photoName = comm.readString();
					String date = comm.readString();
					System.out.println(photoName + " " + date); 
				}
				break;
			case("-c"):
				if(args.length - 3 < index) {
					System.out.println("You did not insert suficient arguments");
					break;
				}

				int commentsIndex = args.length - 2;

				comments = "";
				for(int i = index; i < commentsIndex;i++) {
					comments += args[i] + " ";
				}
				if(comments.equals("")) {
					System.out.println("Can't send empty comments");
					break;
				}
				leaderName = args[args.length - 2];
				photoName = args[args.length - 1];
				comm.writeString("-c");
				comm.writeString(leaderName);
				boolean isFollower1 = comm.readBoolean();
				if(!isFollower1) {
					System.out.println("You need to follow " + leaderName +
							" to comment " + photoName);
					break;
				}

				comm.writeString(photoName);
				boolean photoExists = comm.readBoolean();
				if(!photoExists) {
					System.out.println(leaderName +
							" does not have " + photoName);
					break;
				}
				comm.writeString(comments);


				
			break;

			case("-L"):
				comm.writeString("-L");
				comm.writeString(args[index]);
				notFollower = !comm.readBoolean();
				if(!notFollower){
					comm.writeString(args[index+1]);
					hasPhoto = comm.readBoolean();
					if(hasPhoto){
						Boolean hasLiked = comm.readBoolean();
						if(hasLiked)
							System.out.println("The photo " + args[index+1] +
									" of " + args[index] + " has been liked.");
						else
							System.out.println("You already liked this photo.");
					}
					else{
						System.out.println("There is no photo of " + args[index] + " with that name.");
						break;
					}
				}
				else{
					System.out.println("You need to follow " + args[index] +
							" to like the photos.");
					break;
				}
				
			break;

			case("-D"):
				comm.writeString("-D");
				comm.writeString(args[index]);
				notFollower = !comm.readBoolean();
				if(!notFollower){
					comm.writeString(args[index+1]);
					hasPhoto = comm.readBoolean();
					if(hasPhoto){
						Boolean hasLiked = comm.readBoolean();
						if(hasLiked)
							System.out.println("The photo " + args[index+1] +
									" of " + args[index] + " has been disliked.");
						else
							System.out.println("You already disliked this photo.");
					}
					else{
						System.out.println("There is no photo of " + args[index] + " with that name.");
						break;
					}
				}
				else{
					System.out.println("You need to follow " + args[index] +
							" to dislike the photos.");
					break;
				}
				
			break;

			case("-f"):
				while(index < args.length){
					comm.writeString("-f");
					comm.writeString(args[index]);
					notFollower = comm.readBoolean();
					if(notFollower){
						System.out.println("Added new follower");
						index++;
					}
					else{
						System.out.println(args[index] + " is already a follower");
						break;
					}
					
				}
			break;

			case("-r"):
				while(index < args.length){
					comm.writeString("-r");
					comm.writeString(args[index]);
					notFollower = !comm.readBoolean();
					if(!notFollower){
						System.out.println("You don't follow " + args[index] + " anymore");
						index++;
					}
					else{
						System.out.println("You don't follow " + args[index] +
								" so you can't unfollow");
						break;
					}
					
				}
			break;
			case("-i"):
				comm.writeString("-i");
				if(args.length < (index + 2)) {
					System.out.println("Nao inseriu argumentos suficientes");
					break;
				}
				leaderName = args[index];
				comm.writeString(leaderName);
				isFollower = comm.readBoolean();
				if(!isFollower) {
					System.out.println("You are not following " + leaderName);
					break;
				}
				photoName = args[index +1];
				comm.writeString(photoName);
				boolean hasPhoto1 = comm.readBoolean();
				if(!hasPhoto1) {
					System.out.println(leaderName + " does not have " + photoName);
					break;
				}
				numberOfComments = comm.readInt();
				if(numberOfComments == 0)
					System.out.println("No comments for " + photoName);
				else
					System.out.println("Comments: ");
				for(int i = 0; i < numberOfComments;i++) {
					String comment = comm.readString();
					System.out.println(comment);
				}
				int likes = comm.readInt();
				int dislikes = comm.readInt();
				System.out.println("Likes: " + likes);
				System.out.println("Dislikes: " + dislikes);
			
			break;
			case("-g"):
				comm.writeString("-g");
				if(args.length == index) {
					System.out.println("Not enough arguments");
					break;
				}
				leaderName = args[index];
				comm.writeString(leaderName);
				isFollower =comm.readBoolean();
				if(!isFollower) {
					System.out.println("You are not following " + leaderName);
					break;
				}
				numberOfPhotos = comm.readInt();
				if(numberOfPhotos == 0) {
					System.out.println(leaderName + " does not have photos");
					break;
				}
				for(int i = 0; i < numberOfPhotos; i++) {
					String fileName = comm.readString();
					boolean photoSuccess= comm.receiveFile("download/" + fileName, false);
					if(photoSuccess) {
						numberOfComments = comm.readInt();
						comments = "";
						for(int j = 0; j < numberOfComments;j++) {
							comments += comm.readString();

						}
						comm.writeTextFile("download/" + fileName + " comments.txt", comments);
					}
				}
			}
			
			try {
				comm.writeString("quit");
				closeConnections();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	
	private static void closeConnections() {
		try {
			comm.closeStream();
			echoSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}

