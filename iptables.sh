TRUSTED_MACHINES=10.121.52.14,10.121.52.15,10.101.52.16,10.121.72.23,10.101.85.6,10.101.85.138,10.101.85.18,10.101.148.1,10.101.85.137

sudo /sbin/iptables -F INPUT
sudo /sbin/iptables -F OUTPUT

#Dar permissao as maquinas do lab
sudo /sbin/iptables -A INPUT -s $TRUSTED_MACHINES -j ACCEPT
sudo /sbin/iptables -A OUTPUT -d $TRUSTED_MACHINES -j ACCEPT

#Trafego do dispositivo de loopback
sudo /sbin/iptables -A INPUT -i lo -j ACCEPT
sudo /sbin/iptables -A OUTPUT -o lo -j ACCEPT

#Trafego relacionado com uma ligação jah estabelecida
sudo /sbin/iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
sudo /sbin/iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

#Aceitar trafego proveninte da porta 22
sudo /sbin/iptables -A INPUT -p tcp -s 10.101.149.0/23 --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo /sbin/iptables -A OUTPUT -p tcp -d 10.101.149.0/23 --sport 22 -m state --state NEW,ESTABLISHED -j ACCEPT

#Aceitar ligacoes para o port do nosso servidor
sudo /sbin/iptables -A INPUT -p tcp --dport 23232 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo /sbin/iptables -A OUTPUT -p tcp --sport 23232 -m state --state NEW,ESTABLISHED -j ACCEPT

#Pings a partir do gcc
sudo /sbin/iptables -A OUTPUT -p icmp -d 10.101.151.5 --icmp-type echo-request -m conntrack --ctstate NEW -j ACCEPT
sudo /sbin/iptables -A INPUT -p icmp -s 10.101.151.5 --icmp-type echo-request -m conntrack --ctstate NEW -j ACCEPT

#Ping as maquinas do lab
sudo /sbin/iptables -A OUTPUT -p icmp -d 10.101.149.0/23 --icmp-type echo-request -m conntrack --ctstate NEW -j ACCEPT

#Rejeita o resto
sudo /sbin/iptables -A INPUT -j DROP
sudo /sbin/iptables -A OUTPUT -j DROP