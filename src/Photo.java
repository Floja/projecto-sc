import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Photo {
	private User creator;
	private String name;
	private String extension;
	private String creationDate;
	private ArrayList <String> comments;
	private HashMap<String, Boolean> followersOpinions;
	private int likes;
	private int dislikes;
	
	public Photo(String name,String extension, User creator, String creationDate){
		this.creator = creator;
		this.name = name;
		this.extension = extension;
		this.likes = 0;
		this.dislikes = 0;
		this.creationDate = creationDate;
		followersOpinions = new HashMap<String, Boolean>();
		comments = new ArrayList<String>();
	}
	public Photo(String name, String extension, User creator, String creationDate, int likes, int dislikes, ArrayList<String> comments , HashMap<String, Boolean> opinions){
		this.creator = creator;
		this.name = name;
		this.extension = extension;
		this.likes = likes;
		this.dislikes = dislikes;
		this.creationDate = creationDate;
		this.comments = comments;
		followersOpinions = opinions;
	}
	
	
	public int getLikes() {
		return this.likes;
	}
	public int getDislikes() {
		return this.dislikes;
	}
	public String getDate() {
		return this.creationDate;
	}
	public String getName() {
		return this.name;
	}
	
	public String getExtension() {
		return this.extension;
	}
		
	public boolean addLike(String followerName) {
		
		if(followersOpinions.containsKey(followerName)) {
				//followerName nao gostava da foto
			if(!followersOpinions.get(followerName)) {
				
				dislikes--;
				followersOpinions.replace(followerName, true);
				likes++;
				return true;
			}		
			return false;
		}
		else {
			
			followersOpinions.put(followerName, true);
			likes++;
			return true;
		}
		
	}
	public boolean addDislike(String followerName) {
		if(followersOpinions.containsKey(followerName)) {
			//followerName gostava da foto
			if(followersOpinions.get(followerName)) {
				
				likes--;
				followersOpinions.replace(followerName, false);
				dislikes++;
				return true;
			}		
			return false;
		}
		else {
			
			followersOpinions.put(followerName, false);
			dislikes++;
			return true;
		}
	}
	
	public void addComment(String followerName, String comment) {
		
		this.comments.add(followerName+":"+comment);
		
	}
	
	public HashMap<String,Boolean> getOpinions(){
		return followersOpinions;
		
	}
	
	public ArrayList<String> getComments(){
		return comments;
	}
}
