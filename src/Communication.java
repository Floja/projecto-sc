import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class Communication {
	private static Communication instance = new Communication();
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;
	private CipherOutputStream cos;
	private CipherInputStream cis;
	private Communication() {

	}

	public static Communication getInstance() {
		return instance;
	}
	public void setStream(Socket socket) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		outStream = new ObjectOutputStream(socket.getOutputStream());
		inStream = new ObjectInputStream(socket.getInputStream());
	}
	public void closeStream() throws IOException {
		System.out.println("Closing");
		outStream.close();
		inStream.close();
	}
	public Long readLong() throws IOException {
		return inStream.readLong();
	}
	public void writeLong(Long l) throws IOException {
		outStream.writeLong(l);
	}

	public String readString() throws ClassNotFoundException, IOException {
		return (String) inStream.readObject();
	}

	public void writeString(String i) throws IOException {
		outStream.writeObject(i);
	}

	public int readInt() throws ClassNotFoundException, IOException {
		return (int) inStream.readObject();
	}
	public void writeInt(int i) throws IOException{
		outStream.writeObject(i);
	}


	public boolean readBoolean() throws IOException, ClassNotFoundException {
		return (boolean) inStream.readObject();
	}

	public void writeBoolean(Boolean b) throws IOException {
		outStream.writeObject(b);
	}


	public boolean receiveFile(String fileName, boolean cipher) {
		FileOutputStream outFile = null;

		if(cipher){
			cos = getCipherOutputStream(fileName);
		}else {
			try {
				outFile = new FileOutputStream(fileName);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		final int LIM = 512;
		byte [] buf = new byte [LIM]; 
		double fileSize = 0;
		try {
			fileSize = inStream.readLong();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long bytesRead = 0;
		int counter = 0;
		
		while(bytesRead  < (fileSize - LIM)) {
			try {
				counter = inStream.read(buf, 0, LIM);
			} catch (IOException e) {
				e.printStackTrace();
			}
			bytesRead += counter;
			if(!cipher)
				try {
					outFile.write(buf,0,counter);
				} catch (IOException e) {
					e.printStackTrace();
				}
			else {
				try {
					cos.write(buf,0,counter);
				} catch (IOException e) {
					e.printStackTrace();
				}
			
			}
		}
		
		
		
		try {
			counter = inStream.read(buf, 0, LIM);
		} catch (IOException e) {
			e.printStackTrace();
		}
		bytesRead += counter;
		
		if(!cipher)
			try {
				outFile.write(buf,0,counter);
				outFile.flush();
				outFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		else {
			try {
				cos.write(buf,0,counter);
				cos.flush();
				cos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		
				
		return true;

	} 

	public boolean sendFile(String fileName, boolean cipher) throws IOException {
		try {
			
			File file = new File(fileName);

			if(cipher){
				cis = getCipherInputStream(fileName);	
			}

			
			final int LIM = 512;
			long fileSize = file.length();

			byte [] buffer = new byte [LIM];
			outStream.writeLong(fileSize);
			//send size
			
			FileInputStream inFileStream = new FileInputStream(file);
			int bytesTransfered = 0;
			int counter = 0;

			while(bytesTransfered <= (fileSize - LIM)) {
				if(!cipher){
					counter = inFileStream.read(buffer, 0, LIM);
				}else{
					counter = cis.read(buffer,0,LIM);
				}

				outStream.write(buffer, 0, counter);
				bytesTransfered += counter;
			}
			if(!cipher){
				counter = inFileStream.read(buffer, 0, LIM);
			}else{
				counter = cis.read(buffer,0,LIM);
				outStream.write(buffer, 0, counter);
				counter = cis.read(buffer,0,LIM);
			}
			bytesTransfered += counter;
			if(counter != -1)
				outStream.write(buffer, 0, counter);
			
			if(cipher)
				cis.close();
			
			inFileStream.close();

		} catch(IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void writeTextFile(String fileName, String contents ) throws IOException {
		FileOutputStream outFileStream =new FileOutputStream(fileName);
		outFileStream.write(contents.getBytes());
		outFileStream.close();
	}

	public boolean fileExists(String fileName) {
		File f = new File(fileName);
		return f.exists();
	}

	
	public CipherOutputStream getCipherOutputStream(String fileName) {
		PublicKey pk = getPublicKey();
		Key k = generateKey(fileName, pk);
		return createCipherOutputStream(fileName,k);
	}
	
	public CipherInputStream getCipherInputStream(String fileName) {
		PrivateKey pk = getPrivateKey();
		Key k = readKey(fileName, pk);
		return createCipherInputStream(fileName,k);
	}
	
	
	private Key generateKey(String fileName, PublicKey pk) {
		KeyGenerator kg = null;
		try {
			kg = KeyGenerator.getInstance("AES");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		kg.init(128);
		Key k = kg.generateKey();
		cipherKey(fileName+".key",pk,k);
		return k;
	}
	private void cipherKey(String fileName, PublicKey pk, Key k){
		
		CipherOutputStream cos = createCipherOutputStream(fileName, pk);
		try {
			cos.write(k.getEncoded());
			cos.flush();
			cos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Key readKey(String fileName, PrivateKey pk) {
		
		CipherInputStream cis = createCipherInputStream(fileName + ".key", pk);
		byte keyContents[] = new byte[16];
		try {
			cis.read(keyContents);
			cis.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Key originalKey = new SecretKeySpec(keyContents, 0, 16, "AES");
		
		return originalKey;
	}
	
	
	
	

	private KeyStore getKeyStore() {
		KeyStore ks = null;
		try {
			ks = KeyStore.getInstance("JCEKS");
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// get user password and file input stream
		char[] password = "123456".toCharArray();

		java.io.FileInputStream fis = null;
		try {
			try {
				fis = new java.io.FileInputStream("myServer.keyStore");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				ks.load(fis, password);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return ks;
	}
	
	
	
	private PrivateKey getPrivateKey() {

		KeyStore ks = getKeyStore();
		KeyStore.ProtectionParameter protParam =new KeyStore.PasswordProtection("123456".toCharArray());

		KeyStore.PrivateKeyEntry pkEntry = null;
		try {
			pkEntry = (KeyStore.PrivateKeyEntry) ks.getEntry("myServer", protParam);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableEntryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pkEntry.getPrivateKey();

	}
	public PublicKey getPublicKey() {
		// Get certificate of public key
		KeyStore keyStore = getKeyStore();
		Certificate cert = null;
		try {
			cert = keyStore.getCertificate("myServer");
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Get public key
		return  cert.getPublicKey();
	}
	
	
	private CipherOutputStream createCipherOutputStream(String fileName, Key key) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(fileName);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		Cipher c = null;
		try {
			c = Cipher.getInstance(key.getAlgorithm());
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		}
			try {
				c.init(Cipher.ENCRYPT_MODE, key);
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			
		return new CipherOutputStream(fos, c);
	}
	
	private CipherInputStream createCipherInputStream(String fileName, Key key) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Cipher c = null;
		try {
			c = Cipher.getInstance(key.getAlgorithm());
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			try {
				c.init(Cipher.DECRYPT_MODE, key);
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		return new CipherInputStream(fis, c);
	}

	public void signFile(String fileName) {
		PrivateKey pk = getPrivateKey();
		Signature s = null;
		try {
			s = Signature.getInstance("MD5withRSA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			s.initSign(pk);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader (fileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String currLine = null;
		try {
			currLine = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		while(currLine != null) {
			try {
				
				s.update(currLine.getBytes());
				currLine = br.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SignatureException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		
		try {
			br.close();
			FileOutputStream os  = new  FileOutputStream (fileName + ".sig");
			try {
				os.write(s.sign());
				os.close();
			} catch (SignatureException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public boolean verifySign(String fileName) {
			PublicKey pk = getPublicKey( );
			Signature s = null;
			try {
				s = Signature.getInstance("MD5withRSA");
			} catch (NoSuchAlgorithmException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				s.initVerify(pk);
			} catch (InvalidKeyException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader (fileName));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String currLine = null;
			try {
				currLine = br.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			while(currLine != null) {
				try {
					
					s.update(currLine.getBytes());
					currLine = br.readLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SignatureException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			try {
				br.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			File f = new File(fileName+".sig");
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(fileName+".sig");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			byte signature[] = new byte[(int)f.length()];
			try {
				fis.read(signature);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				return s.verify(signature);
			} catch (SignatureException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return false;
	}
	
	
	
	
	
}

